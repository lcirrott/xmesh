/* =============================================================================
**  This file is part of the xmesh software package for conservative solution
**  projection over simplicial mesh intersections.
**  Copyright (c) Luca Cirrottola, 2021-
**
**  xmesh is free software: you can redistribute it and/or modify it
**  under the terms of the GNU Lesser General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  xmesh is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License and of the GNU General Public License along with parmmg (in
**  files COPYING.LESSER and COPYING). If not, see
**  <http://www.gnu.org/licenses/>. Please read their terms carefully and
**  use this copy of the parmmg distribution only if you accept them.
** =============================================================================
*/

/**
 * \file libxmeshtypes.h
 * \brief xmesh types and functions that must be accessible to the library users
 * \author Luca Cirrottola (Inria)
 * \version 1
 * \copyright GNU Lesser General Public License.
 */

#ifndef LIBXMESHTYPES_HPP
#define LIBXMESHTYPES_HPP

#include <cstdint>
#include <algorithm>
#include <cassert>
#include <cstring>

#define XMESH_delete 255

using namespace std;

/**
 * Class for intersecting meshes.
 */
class xmesh_t{
};

/**
 * Class for polar coordinates on a plane face.
 */
struct polar_t{
  uint8_t v;
  double theta;
};

/**
 * Class for a polyhedron face.
 *
 * Default constructor: the maximum number of vertices is fixed, the intial
 * number of vertices is 0 and the vertices array is uniformly initialized to 0.
 */
template<uint8_t ndim>
class face_t{
  /* Maximum number of vertices */
  static constexpr uint8_t nvmax = {(ndim-2) ? 8 : 7}; /* 7 in 2D, 8 in 3D */
  public:
  /* Number of vertices */
  uint8_t nv{};
  /* Vertices */
  uint8_t v[nvmax]{};
  /* Destructor */
  ~face_t();
};

/**
 * Destructor for the face class.
 * Set number of vertices and vertices to 0.
 */
template<uint8_t ndim>
face_t<ndim>::~face_t(){
  nv = 0;
  memset(v,0,nvmax*sizeof(uint8_t));
}

/**
 * Pointer to a polyhedron face class.
 */
template<uint8_t ndim>
using pface_t = face_t<ndim> *;

/**
 * Class for a polyhedron.
 */
template<uint8_t ndim>
class poly_t{
  /* Maximum number of points */
  static constexpr uint8_t npmax = {ndim*(ndim+1)}; /* 6 in 2D, 12 in 3D */
  /* Maximum number of faces */
  static constexpr uint8_t nfmax = {(ndim-2) ? 2*(ndim+1) : 1}; /* 1 in 2D, 8 in 3D */
  /* Maximum number of temporary points */
  static constexpr uint8_t ntmpmax = {(ndim-2) ? nfmax-1 : 2}; /* 2 in 2D, 7 in 3D */
  /* Levelset tolerance */
  static constexpr double eps = 1.0e-12;
  /* Points coordinates */
  double coor[npmax+ntmpmax][ndim];   /* (6+2)*2 in 2D,  (12+7)*3 in 3D */
  /* Levelset nodal values */
  double m[npmax+ntmpmax];            /* 6+2 in 2D,  12+7 in 3D */
  /* Normal vectors to the faces */
  double normal[nfmax][ndim]; /* 1*2 in 2D,  8*3 in 3D */
  /* Edge table */
  uint8_t edge[npmax][npmax]; /* 6*6 in 2D, 12*12 in 3D */
  /* Face objects */
  face_t<ndim> face[nfmax];
  /* Number of points */
  uint8_t np{};
  /* Number of faces */
  uint8_t nf{};
  /* Get edge */
  uint8_t* get_edge(uint8_t a,uint8_t b){ return &edge[a][b]; };
  /* Reset edges */
  void reset_edges();
  /* Compute levelset */
  uint8_t levelset(double *normal,double *c0);
  /* Insert new point */
  void newpoint(uint8_t ip0,uint8_t ip1,uint8_t newp);
  /* Split edge */
  void split(uint8_t ifac,uint8_t ip0,uint8_t ip1,uint8_t *newp,uint8_t *newv);
  /* Pack polyhedron */
  void pack();
  /* Sort face vertices */
  void sort();
  /* Cut polyhedron with respect to a plane */
  uint8_t cut(double *normal,double *c0);
  public:
  /* Constructor */
  poly_t();
  /* Destructor */
  ~poly_t();
  /* Tests */
  uint8_t test_cut();
};

/**
 * Constructor for the polyhedron class.
 * Recognize unused edges by the XMESH_delete tag.
 */
template<uint8_t ndim>
poly_t<ndim>::poly_t(){
  static_assert((ndim == 2) || (ndim == 3),
      "Only two-dimensional or three-dimensional polyhedra can be created.");
  reset_edges();
  for( uint8_t ifac = 0; ifac < nf; ifac++ )
    face[ifac].~face_t();
  np = 0;
  nf = 0;
}

/**
 * Destructor for the polyhedron class.
 * Recognize unused edges by the XMESH_delete tag.
 */
template<uint8_t ndim>
poly_t<ndim>::~poly_t(){
  np = nf = 0;
  reset_edges();
}

template<uint8_t ndim>
void poly_t<ndim>::reset_edges(){
  memset(edge,XMESH_delete,npmax*npmax*sizeof(uint8_t));
}

template<uint8_t ndim>
uint8_t poly_t<ndim>::levelset(double normal[ndim],double c0[ndim]){
  uint8_t cut;
  cut = 0;
  for( uint8_t ip = 0; ip < np; ip++ ){
    m[ip] = 0.0;
    for( uint8_t d = 0; d < ndim; d++ ){
      m[ip] += (coor[ip][d]-c0[d])*normal[d];
    }
    if( m[ip] < eps )
      cut = 1;
  }

  return cut;
}

template<uint8_t ndim>
void poly_t<ndim>::newpoint(uint8_t ip0,uint8_t ip1,uint8_t newp){
  double s1,s0;

  s1 = m[ip0]/(m[ip0]-m[ip1]);
  assert(isfinite(s1));
  s0 = 1.0-s1;

  for( uint8_t d = 0; d < ndim; d++ ){
    coor[newp][d] = s0*coor[ip0][d]+s1*coor[ip1][d];
  }
  m[newp] = 0.0;
}

template<uint8_t ndim>
void poly_t<ndim>::split(uint8_t ifac,uint8_t ip0,uint8_t ip1,uint8_t *newp,uint8_t *newv){
  uint8_t *edge_i,imin,imax;

  imin = min(ip0,ip1);
  imax = max(ip0,ip1);
  edge_i = get_edge(imin,imax);
  if( *edge_i == npmax*imin+imax ){
    if( m[ip0]*m[ip1] < -eps ){ /* split edge */
      /* insert new point coordinates */
      newpoint(ip0,ip1,*newp);
      /* register new edge */
      *edge_i = *newp;
      /* register point on this face and increment point counter */
      face[ifac].v[face[ifac].nv+(*newv)++] = (*newp)++;
    }
  } else { /* get split point */
    /* edge already split, register point on this face */
    face[ifac].v[face[ifac].nv+(*newv)++] = *edge_i;
  }
}

template<uint8_t ndim>
void poly_t<ndim>::pack(){
  uint8_t map[npmax];
  uint8_t pos,jp0,jp1,imin,imax,jmin,jmax;
  uint8_t *edge_i,*edge_j;
  pface_t<ndim> pface;

  for( uint8_t ip = 0; ip < np; ip++ ){
    map[ip] = ip;
  }

  /* pack points */
  pos = 0;
  do {
    if( m[pos] > eps ){ /* positive points are outside */
      /* find last valid point */
      while( np > pos && m[np-1] > eps ){
        map[(np--)-1] = XMESH_delete;
      }
      if( np > pos ){
        memcpy(coor[pos],coor[np-1],ndim*sizeof(double));
        map[pos] = XMESH_delete;
        map[(np--)-1] = pos;
      }
    }
  } while ( ++pos < np );

  /* pack face vertices */
  for( uint8_t ifac = 0; ifac < nf; ifac++ ){
    pface = &face[ifac];
    pos = 0;
    do {
      if( map[pface->v[pos]] == XMESH_delete ){
        /* move vertex and decrement number of vertices */
        pface->v[pos] = pface->v[(pface->nv--)-1];
      }
      /* get correct vertex index */
      pface->v[pos] = map[pface->v[pos]];
    } while( ++pos < pface->nv );
    /* mark face for deletion if it has less than 3 vertices */
    if( pface->nv < 3 )
      pface->nv = XMESH_delete;
  }

  /* pack faces */
  pos = 0;
  do {
    pface = &face[pos];
    if( pface->nv == XMESH_delete ) {
      /* find last valid face */
      while( nf > pos && face[nf-1].nv == XMESH_delete ){
        nf--;
      }
      if( nf > pos ){
        /* move face and decrement number of faces */
        memcpy(&face[pos],&face[(nf--)-1],sizeof(face_t<ndim>));
      }
    }
  } while( ++pos < nf );

}

static inline int compare( const void *a, const void *b ){
  polar_t *pa;
  polar_t *pb;

  pa = (polar_t *)a;
  pb = (polar_t *)b;

  if( pa->theta < pb->theta )
    return -1;
  if( pa->theta > pb->theta )
    return 1;

  return 0;
}

template<uint8_t ndim>
void poly_t<ndim>::sort(){
  polar_t polar[2*ndim+1];
  pface_t<ndim> pface;
  double b[ndim],axisx[ndim],axisy[ndim],ray[ndim],norm,dd,prox,proy;
  uint8_t ip;

  for( uint8_t ifac = 0; ifac < nf; ifac++ ){
    pface = &face[ifac];
    /* compute barycenter and abscissa axis */
    for( uint8_t d = 0; d < ndim; d++ ){
      b[d] = 0.0;
      for( uint8_t i = 0; i < pface->nv; i++ )
        b[d] += coor[pface->v[i]][d];
    }
    norm = 1.0/pface->nv;
    dd = 0.0;
    for( uint8_t d = 0; d < ndim; d++ ){
      b[d] *= norm;
      axisx[d] = coor[pface->v[0]][d]-b[d];
      dd += axisx[d]*axisx[d];
    }
    dd = 1.0/sqrt(dd);
    for( uint8_t d = 0; d < ndim; d++ )
      axisx[d] *= dd;
    if( ndim == 3 ){
      axisy[0] = normal[ifac][1]*axisx[2]-normal[ifac][2]*axisx[1];
      axisy[1] = normal[ifac][2]*axisx[0]-normal[ifac][0]*axisx[2];
      axisy[2] = normal[ifac][0]*axisx[1]-normal[ifac][1]*axisx[0];
    } else if(ndim == 2){
      axisy[0] = -axisx[1];
      axisy[1] = axisx[0];
    }
    dd = 0.0;
    for( uint8_t d = 0; d < ndim; d++ )
      dd += axisy[d]*axisy[d];
    dd = 1.0/sqrt(dd);
    for( uint8_t d = 0; d < ndim; d++ )
      axisy[d] *= dd;

    /* compute polar angle */
    polar[0].v = pface->v[0];
    polar[0].theta = 0.0; /* the first point is on the axis */
    for( uint8_t i = 1; i < pface->nv; i++ ){
      ip = pface->v[i];
      norm = 0.0;
      prox = 0.0;
      proy = 0.0;
      /* compute projections */
      for( uint8_t d = 0; d < ndim; d++ ){
        ray[d] = coor[ip][d]-b[d];
        norm += axisx[d]*axisx[d];
        prox +=  ray[d]*axisx[d];
      }
      norm = sqrt(norm);
      for( uint8_t d = 0; d < ndim; d++ ){
        ray[d] -= prox/norm*axisx[d];
        proy   += ray[d]*axisy[d];
      }
      /* compute polar angle */
      polar[i].v = pface->v[i];
      polar[i].theta = atan2(proy,prox);
    }
    /* sort vertices */
    qsort(polar,pface->nv,sizeof(polar_t),compare);
    for( uint8_t i = 0; i < pface->nv; i++ ){
      pface->v[i] = polar[i].v;
    }
  }


  /* rebuild edge table */
  reset_edges();
  uint8_t imin,imax,*edge_i;
  for( uint8_t ifac = 0; ifac < nf; ifac++ ){
    pface = &face[ifac];
    for( uint8_t i = 0; i < pface->nv-1; i++ ){
      imin = min(pface->v[i],pface->v[i+1]);
      imax = max(pface->v[i],pface->v[i+1]);
      edge_i = get_edge(imin,imax);
      *edge_i = npmax*imin+imax;
    }
    imin = min(pface->v[pface->nv-1],pface->v[0]);
    imax = max(pface->v[pface->nv-1],pface->v[0]);
    edge_i = get_edge(imin,imax);
    *edge_i = npmax*imin+imax;
  }
}

template<uint8_t ndim>
uint8_t poly_t<ndim>::cut(double *plane,double *c0){
  pface_t<ndim> pface;

  /* Compute level-set function */
  if( !levelset(plane,c0) )
    return 0;

  /*
   * Keep points:               m < -eps
   * Capture points:     -eps < m < +eps
   * Delete points:             m > +eps
   *
   */

  /* Insert new points by looping on face edges */
  uint8_t newp,newv;
  newp = np;
  for( uint8_t ifac = 0; ifac < nf; ifac++ ){
    pface = &face[ifac];
    newv = 0;
    for( uint8_t i = 0; i < pface->nv-1; i++ ){
      split(ifac,pface->v[i],pface->v[i+1],&newp,&newv);
    }
    /* last edge */
    split(ifac,pface->v[pface->nv-1],pface->v[0],&newp,&newv);
    /* increase points counter on face */
    pface->nv += newv;
  }
  /* update number of points */
  np = newp;


  /* Insert captured and deleted points on last face */
  pface = &face[nf];
  pface->nv = 0;
  for( uint8_t ip = 0; ip < np; ip++ ){ /* insert vertices */
    if( m[ip] > -eps ){
      pface->v[pface->nv++] = ip;
    }
  }
  /* insert normal and increase faces counter */
  for( uint8_t d = 0; d < ndim; d++ )
    normal[nf][d] = plane[d];
  nf++;


  /* pack */
  pack();

  /* sort */
  sort();

  return 1;
}

template<>
inline uint8_t poly_t<3>::test_cut(){
  double plane[4][3],c0[4][3],c1[4][3],t[3];

  /* translation */
  t[0] = -0.0;
  t[1] = -0.0;
  t[2] =  0.5;

  c1[0][0] =  0.5+t[0];
  c1[0][1] =  0.5+t[1];
  c1[0][2] =  0.5+t[2];
  c1[1][0] =  0.5+t[0];
  c1[1][1] = -0.5+t[1];
  c1[1][2] =  0.5+t[2];
  c1[2][0] = -0.5+t[0];
  c1[2][1] =  0.5+t[1];
  c1[2][2] =  0.5+t[2];
  c1[3][0] =  0.5+t[0];
  c1[3][1] =  0.5+t[1];
  c1[3][2] = -0.5+t[2];
  plane[0][0] = -1.0;
  plane[0][1] = -1.0;
  plane[0][2] = -1.0;
  plane[1][0] =  0.0;
  plane[1][1] =  1.0;
  plane[1][2] =  0.0;
  plane[2][0] =  1.0;
  plane[2][1] =  0.0;
  plane[2][2] =  0.0;
  plane[3][0] =  0.0;
  plane[3][1] =  0.0;
  plane[3][2] =  1.0;



  c0[0][0] = 0.0;
  c0[0][1] = 0.0;
  c0[0][2] = 0.0;
  c0[1][0] = 1.0;
  c0[1][1] = 0.0;
  c0[1][2] = 0.0;
  c0[2][0] = 0.0;
  c0[2][1] = 1.0;
  c0[2][2] = 0.0;
  c0[3][0] = 0.0;
  c0[3][1] = 0.0;
  c0[3][2] = 1.0;


  np = 4;
  for( uint8_t ip = 0; ip < 4; ip++ )
    for( uint8_t d = 0; d < 3; d++ )
      coor[ip][d] = c0[ip][d];
  nf = 0;
  face[nf].nv = 3;
  face[nf].v[0] = 1;
  face[nf].v[1] = 2;
  face[nf].v[2] = 3;
  normal[nf][0] = 1.0;
  normal[nf][1] = 1.0;
  normal[nf][2] = 1.0;
  nf++;
  face[nf].nv = 3;
  face[nf].v[0] = 2;
  face[nf].v[1] = 3;
  face[nf].v[2] = 0;
  normal[nf][0] = -1.0;
  normal[nf][1] =  0.0;
  normal[nf][2] =  0.0;
  nf++;
  face[nf].nv = 3;
  face[nf].v[0] = 3;
  face[nf].v[1] = 0;
  face[nf].v[2] = 1;
  normal[nf][0] =  0.0;
  normal[nf][1] = -1.0;
  normal[nf][2] =  0.0;
  nf++;
  face[nf].nv = 3;
  face[nf].v[0] = 0;
  face[nf].v[1] = 1;
  face[nf].v[2] = 2;
  normal[nf][0] =  0.0;
  normal[nf][1] =  0.0;
  normal[nf][2] = -1.0;
  nf++;

  for( uint8_t ip0 = 0; ip0 < np; ip0++ )
    for( uint8_t ip1 = ip0+1; ip1 < np; ip1++ )
      edge[ip0][ip1] = npmax*ip0+ip1;


  /* sequential cuts */
  uint8_t cutOK,it = 0;
  do {
    cutOK = cut(plane[it],c1[(it+1)%4]);
    it++;
  } while( cutOK && it < 4 );
  if( !cutOK ) {
    nf = 0;
  }
  it--;


  FILE *fid;
  fid = fopen("cut.mesh","w");
  fprintf(fid,"MeshVersionFormatted\n 2\n");
  fprintf(fid,"\nDimension\n 3\n");
  uint8_t nppoly = np+nf;
  if( nf ) nppoly++; /* add volume barycenter */
  fprintf(fid,"\nVertices\n %d\n",nppoly+8);
  for( uint8_t ip = 0; ip < np; ip++ )
    fprintf(fid,"%f %f %f 2\n",coor[ip][0],coor[ip][1],coor[ip][2]);

  uint8_t ntetra = 0;
  double bv[3];
  bv[0] = bv[1] = bv[2] = 0.0;
  for( uint8_t ifac = 0; ifac < nf; ifac++ ){
    pface_t<3> pface = &face[ifac];
    double bf[3];
    bf[0] = bf[1] = bf[2] = 0.0;
    ntetra += pface->nv;
    for(uint8_t d = 0; d < 3; d++ ){
      for( uint8_t i = 0; i < pface->nv; i++ )
        bf[d] += coor[pface->v[i]][d];
      bf[d] *= 1.0/pface->nv;
    }
    fprintf(fid,"%f %f %f 3\n",bf[0],bf[1],bf[2]);
    for(uint8_t d = 0; d < 3; d++ )
      bv[d] += bf[d];
  }
  if( nf ) {
    for(uint8_t d = 0; d < 3; d++ )
      bv[d] *= 1.0 / nf;
    fprintf(fid,"%f %f %f 3\n",bv[0],bv[1],bv[2]);
  }
  for(uint8_t ip = 0; ip < 4; ip++ )
    fprintf(fid,"%f %f %f 0\n",c0[ip][0],c0[ip][1],c0[ip][2]);
  for(uint8_t ip = 0; ip < 4; ip++ )
    fprintf(fid,"%f %f %f 1\n",c1[ip][0],c1[ip][1],c1[ip][2]);



  fprintf(fid,"\nTetrahedra\n %d\n",ntetra+2);
  for( uint8_t ifac = 0; ifac < nf; ifac++ ){
    pface_t<3> pface = &face[ifac];
    for( uint8_t i = 0; i < pface->nv-1; i++ )
      fprintf(fid,"%d %d %d %d %d\n",pface->v[i]+1,pface->v[i+1]+1,np+ifac+1,np+nf+1,it);
    fprintf(fid,"%d %d %d %d %d\n",pface->v[pface->nv-1]+1,pface->v[0]+1,np+ifac+1,np+nf+1,it);
  }
  fprintf(fid,"%d %d %d %d 4\n",nppoly+1,nppoly+2,nppoly+3,nppoly+4);
  fprintf(fid,"%d %d %d %d 5\n",nppoly+5,nppoly+6,nppoly+7,nppoly+8);


  uint8_t na = 0;
  for( uint8_t ip0 = 0; ip0 < np; ip0++ )
    for( uint8_t ip1 = ip0+1; ip1 < np; ip1++ )
      if(edge[ip0][ip1] != XMESH_delete )
        na++;
  fprintf(fid,"\nEdges\n %d\n",na);
  for( uint8_t ip0 = 0; ip0 < np; ip0++ )
    for( uint8_t ip1 = ip0+1; ip1 < np; ip1++ )
      if(edge[ip0][ip1] != 255 )
        fprintf(fid,"%d %d %d\n",ip0+1,ip1+1,it);
//  uint8_t na = 0;
//  for( uint8_t ifac = 0; ifac < nf; ifac++ )
//    na += face[ifac].nv;
////  na = na/2;
//  fprintf(fid,"\nEdges\n %d\n",na);
//  for( uint8_t ifac = 0; ifac < nf; ifac++ ){
//    for( uint8_t i = 0; i < face[ifac].nv-1; i++ )
//      fprintf(fid,"%d %d 0\n",face[ifac].v[i]+1,face[ifac].v[i+1]+1);
//    fprintf(fid,"%d %d 0\n",face[ifac].v[face[ifac].nv-1]+1,face[ifac].v[0]+1);
//  }
  fprintf(fid,"\nRidges\n %d\n",na);
  for( uint8_t ia = 0; ia < na; ia++ )
    fprintf(fid,"%d\n",ia+1);
  fprintf(fid,"\nEnd");
  fclose(fid);

  return 1;
}

#endif /* LIBXMESHTYPES_HPP */
