if( BUILD_TESTING )
  include( CTest )

  if( LIBXMESH_STATIC )
    set( lib_name lib${PROJECT_NAME}_a )
    set( LIB_TESTS ON )
  elseif( LIBXMESH_SHARED )
    set( LIB_TESTS ON )
    set( lib_name lib${PROJECT_NAME}_so )
  endif( )


  set( XMESH_TESTS
    test0
    )

  set( XMESH_TESTS_MAIN_PATH
    ${PROJECT_SOURCE_DIR}/libexamples/test.cpp
    )

  list(LENGTH XMESH_TESTS nbTests_tmp)
  math(EXPR nbTests "${nbTests_tmp} - 1")

  foreach( test_idx RANGE ${nbTests} )
    list( GET XMESH_TESTS ${test_idx} test_name )
    list( GET XMESH_TESTS_MAIN_PATH  ${test_idx} main_path )

    add_library_test( ${test_name} ${main_path} "copy_xmesh_headers" "${lib_name}" )

    add_test( NAME ${test_name} COMMAND
      $<TARGET_FILE:${test_name}>
      )
  endforeach()

endif()
