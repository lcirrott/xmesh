###############################################################################
#####
#####         Copy an automatically generated header file to another place
#####
###############################################################################
macro( copy_header
    in_dir in_file out_dir out_file
    file_dependencies
    target_name
    )
  # Wrap add_custom_command into add_custom target to remove dpendencies from
  # the custom command and thus allow parallel build.
  add_custom_command(
    OUTPUT  ${out_dir}/${out_file}
    COMMAND ${CMAKE_COMMAND} -E copy  ${in_dir}/${in_file} ${out_dir}/${out_file}
    WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
    DEPENDS ${file_dependencies} ${in_dir}/${in_file}
    COMMENT "Copying ${in_dir}/${in_file} in ${out_dir}/${out_file}"
    )

  add_custom_target( ${target_name} ALL
    DEPENDS ${out_dir}/${out_file} )
  add_dependencies( ${target_name} ${file_dependencies} )

endmacro()

###############################################################################
#####
#####         Copy source and automatically generated header files to another
#####         place and create the associated target
#####
###############################################################################
macro( copy_headers_and_create_target
    source_dir binary_dir include_dir )

  add_custom_target(xmeshtypes_header ALL
    DEPENDS
    ${source_dir}/libxmeshtypes.hpp )

  copy_header(
    ${source_dir} libxmeshtypes.hpp
    ${include_dir} libxmeshtypes.hpp
    xmeshtypes_header copy_libxmeshtypes )

  set( tgt_list
    copy_libxmeshtypes )

  add_custom_target(copy_xmesh_headers ALL
    DEPENDS ${tgt_list})

endmacro()

###############################################################################
#####
#####         Add a library to build and needed include dir, set its
#####         properties, add link dependencies and the install rule
#####
###############################################################################

macro( add_and_install_library
    target_name target_type sources output_name )

  add_library( ${target_name} ${target_type} ${sources} )
  add_library( xmesh::${target_name} ALIAS ${target_name} )

  if( CMAKE_VERSION VERSION_LESS 2.8.12 )
    include_directories( ${target_name} PRIVATE
      ${COMMON_BINARY_DIR} ${COMMON_SOURCE_DIR} ${CMAKE_BINARY_DIR}/include )
  else()
    target_include_directories( ${target_name} PUBLIC
      $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/include/>
      $<BUILD_INTERFACE:${COMMON_SOURCE_DIR}>
      $<BUILD_INTERFACE:${COMMON_BINARY_DIR}>
      $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}> )
  endif()

  set_target_properties( ${target_name}
    PROPERTIES OUTPUT_NAME ${output_name} )

  set_property(TARGET ${target_name} PROPERTY C_STANDARD 99)

  target_link_libraries( ${target_name} ${LIBRARIES} )

  install(TARGETS ${target_name} EXPORT xmeshTargets
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
    INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
    Component lib
    )
endmacro()

###############################################################################
#####
#####         Add an executable to build and needed include dir, set its
#####         postfix, add link dependencies and the install rule
#####
###############################################################################

macro( add_and_install_executable
    exec_name main_file )

  # get library files as extra argument (standard arguments cannot by unset)
  set( lib_files ${ARGN} )

  if( NOT TARGET lib${exec_name}_a AND NOT TARGET lib${exec_name}_so )
    if( lib_files )
      add_executable( ${exec_name} ${lib_files} ${main_file} )
    else()
      add_executable( ${exec_name} ${main_file})
    endif()
  else()
    add_executable( ${exec_name} ${main_file})

    set_property(TARGET ${exec_name} PROPERTY C_STANDARD 99)

    if( NOT TARGET lib${exec_name}_a )
      target_link_libraries(${exec_name} lib${exec_name}_so)
    else()
      target_link_libraries(${exec_name} lib${exec_name}_a)
    endif()

  endif()

  #  if( WIN32 AND NOT MINGW and SCOTCH_FOUND )
  #    my_add_link_flags ( ${exec_name} "/SAFESEH:NO")
  #  endif()

  if( CMAKE_VERSION VERSION_LESS 2.8.12 )
    include_directories( ${exec_name} PUBLIC
      ${COMMON_BINARY_DIR} ${COMMON_SOURCE_DIR} ${PROJECT_BINARY_DIR}/include )
  else()
    target_include_directories( ${exec_name} PUBLIC
      $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/include/>
      $<BUILD_INTERFACE:${COMMON_SOURCE_DIR}>
      $<BUILD_INTERFACE:${COMMON_BINARY_DIR}>
      $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}> )
  endif()

  target_link_libraries( ${exec_name} ${LIBRARIES}  )

  install(TARGETS ${exec_name}
    EXPORT xmeshTargets RUNTIME DESTINATION bin COMPONENT appli)

  add_target_postfix(${exec_name})

ENDMACRO()

###############################################################################
#####
#####         Add a target postfix depending on the build type
#####
###############################################################################

macro( add_target_postfix target_name )
  if( CMAKE_BUILD_TYPE MATCHES "Debug" )
    # in debug mode we name the executable xmesh_debug
    set_target_properties(${target_name} PROPERTIES DEBUG_POSTFIX _debug)
  elseif( CMAKE_BUILD_TYPE MATCHES "Release" )
    # in Release mode we name the executable xmesh_O3
    set_target_properties(${target_name} PROPERTIES RELEASE_POSTFIX _O3)
  elseif( CMAKE_BUILD_TYPE MATCHES "RelWithDebInfo" )
    # in RelWithDebInfo mode we name the executable xmesh_O3d
    set_target_properties(${target_name} PROPERTIES RELWITHDEBINFO_POSTFIX _O3d)
  elseif( CMAKE_BUILD_TYPE MATCHES "MinSizeRel" )
    # in MinSizeRel mode we name the executable xmesh_s
    set_target_properties(${target_name} PROPERTIES MINSIZEREL_POSTFIX _Os)
  endif()
endmacro()

###############################################################################
#####
#####         Add a library test
#####
###############################################################################

macro( ADD_LIBRARY_TEST target_name main_path target_dependency lib_name )
  add_executable( ${target_name} ${main_path} )
  add_dependencies( ${target_name} ${target_dependency} )

  if( CMAKE_VERSION VERSION_LESS 2.8.12 )
    include_directories ( ${target_name} PUBLIC ${CMAKE_BINARY_DIR}/include )
  else()
    TARGET_INCLUDE_DIRECTORIES ( ${target_name} PUBLIC ${CMAKE_BINARY_DIR}/include )
  endif()

  #  if( WIN32 AND ((NOT MINGW) AND SCOTCH_FOUND) )
  #    MY_ADD_LINK_FLAGS ( ${target_name} "/SAFESEH:NO" )
  #  endif()

  target_link_libraries( ${target_name}  ${lib_name} )
  install(TARGETS ${target_name} RUNTIME DESTINATION bin COMPONENT appli )

endmacro()
