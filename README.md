# xmesh ("Crossmesh")

[![pipeline status](https://gitlab.inria.fr/lcirrott/xmesh/badges/master/pipeline.svg?key_text=pipeline:+master&key_width=94)](https://gitlab.inria.fr/lcirrott/xmesh/commits/master)
[![pipeline status](https://gitlab.inria.fr/lcirrott/xmesh/badges/develop/pipeline.svg?key_text=pipeline:+develop&key_width=100)](https://gitlab.inria.fr/lcirrott/xmesh/commits/develop)

Mesh intersection and conservative solution projection.

<p float="left">
  <img src="doc/cut_face.png" width="400" />
  <img src="doc/cut_noface.png" width="400" />
</p>

Mainly inspired by [this paper by P.E. Farrel and J.R. Maddison (preprint link)](https://www.ljll.math.upmc.fr/~frey/papers/divers/Farrell%20P.E.,%20Conservative%20interpolation%20between%20volume%20meshes%20by%20local%20Galerkin%20projection.pdf), plus some original tweaks.

Objectives:
1. Provide library functions for intersecting an adapted mesh with a background mesh. Both meshes are simplicial ones (triangles in 2D, tetrahedra in 3D).
2. Provide library functions to conservatively interpolate a solution field on the computed mesh intersections.

## Warning

_Please do not shoot the pianist_. This project is not funded. It relies on the authors' free time. So no time planning exists.

In order for the project to progress with sporadic nightly contributions (if any), all development will stricly follow an incremental approach based on these steps: 1) project skeleton, 2) element intersections, 3) CI platform, 4) numerical quadrature, 5) FEM basis, 6) full mesh handling, 7) full solution field interpolation.

Current implementation status: 2.5/7.
