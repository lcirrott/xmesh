/* =============================================================================
**  This file is part of the xmesh software package for conservative solution
**  projection over simplicial mesh intersections.
**  Copyright (c) Luca Cirrottola (Inria), 2021-
**
**  xmesh is free software: you can redistribute it and/or modify it
**  under the terms of the GNU Lesser General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  xmesh is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License and of the GNU General Public License along with parmmg (in
**  files COPYING.LESSER and COPYING). If not, see
**  <http://www.gnu.org/licenses/>. Please read their terms carefully and
**  use this copy of the parmmg distribution only if you accept them.
** =============================================================================
*/

/**
 * \file test.cpp
 * \brief test program
 * \author Luca Cirrottola (Inria)
 * \version 1
 * \copyright GNU Lesser General Public License.
 */

#include <iostream>
#include "libxmeshtypes.hpp"

using namespace std;

int main(int argc,char *argv[]){
  xmesh_t xmesh;
  poly_t<3> poly;

  cout << "XMESH up and running." << endl;
  poly.test_cut();

  return EXIT_SUCCESS;
}
