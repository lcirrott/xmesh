#!/usr/bin/env bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --time=00:10:00
#SBATCH --partition=routage
#SBATCH --output=./build/log1_ctest_plafrim
#SBATCH --error=./build/log2_ctest_plafrim
#SBATCH --wait

guix environment --pure cmake
cd build
ctest --no-tests=error -V
