#!/bin/bash

# Submit jobs
NJOB=0
JOB_ID=`sbatch $CI_PROJECT_DIR/testing/plafrim/job.sh \
        | sed "s#Submitted batch job ##"`
if [[ -n "$JOB_ID" ]]
then
  JOB_LIST="JOB_LIST $JOB_ID"
  NJOB=$[NJOB+1]
fi

cat $CI_PROJECT_DIR/build/log1_ctest_plafrim
cat $CI_PROJECT_DIR/build/log2_ctest_plafrim
